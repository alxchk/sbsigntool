/*
 * Copyright (C) 2012 Jeremy Kerr <jeremy.kerr@canonical.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the OpenSSL
 * library under certain conditions as described in each individual source file,
 * and distribute linked combinations including the two.
 *
 * You must obey the GNU General Public License in all respects for all
 * of the code used other than OpenSSL. If you modify file(s) with this
 * exception, you may extend this exception to your version of the
 * file(s), but you are not obligated to do so. If you do not wish to do
 * so, delete this exception statement from your version. If you delete
 * this exception statement from all source files in the program, then
 * also delete it here.
 */
#define _GNU_SOURCE

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/statfs.h>
#include <sys/types.h>

#include <getopt.h>

#include <efi.h>

#include <ccan/list/list.h>
#include <ccan/array_size/array_size.h>
#include <ccan/talloc/talloc.h>

#include <openssl/x509.h>
#include <openssl/err.h>

#include "fileio.h"
#include "efivars.h"

#define EFIVARS_MOUNTPOINT	"/sys/firmware/efi/efivars"
#define PSTORE_FSTYPE		0x6165676C
#define EFIVARS_FSTYPE		0xde5e81e4

#define EFI_IMAGE_SECURITY_DATABASE_GUID \
	{ 0xd719b2cb, 0x3d3a, 0x4596, \
	{ 0xa3, 0xbc, 0xda, 0xd0, 0x0e, 0x67, 0x65, 0x6f } }

static const char *toolname = "sbkeysync";

static const uint32_t sigdb_attrs = EFI_VARIABLE_NON_VOLATILE |
	EFI_VARIABLE_BOOTSERVICE_ACCESS |
	EFI_VARIABLE_RUNTIME_ACCESS |
	EFI_VARIABLE_TIME_BASED_AUTHENTICATED_WRITE_ACCESS |
	EFI_VARIABLE_APPEND_WRITE;

struct key_database_type {
	const char	*name;
	EFI_GUID	guid;
};

struct key_database_type keydb_types[] = {
	{ "PK",  EFI_GLOBAL_VARIABLE },
	{ "KEK", EFI_GLOBAL_VARIABLE },
	{ "db",  EFI_IMAGE_SECURITY_DATABASE_GUID },
	{ "dbx", EFI_IMAGE_SECURITY_DATABASE_GUID },
};

enum keydb_type {
	KEYDB_PK = 0,
	KEYDB_KEK = 1,
	KEYDB_DB = 2,
	KEYDB_DBX = 3,
};

struct key {
	EFI_GUID			type;
	int				id_len;
	uint8_t				*id;

	char				*description;
	char				*data;
	uint32_t			data_len;

	struct list_node		list;

	/* set for keys loaded from a filesystem keystore */
	struct fs_keystore_entry	*keystore_entry;
};

typedef int (*key_parse_func)(struct key *, uint8_t *, size_t);

struct cert_type {
	EFI_GUID	guid;
	key_parse_func	parse;
};

struct key_database {
	const struct key_database_type	*type;
	struct list_head		keys;
};

struct keyset {
	struct key_database	pk;
	struct key_database	kek;
	struct key_database	db;
	struct key_database	dbx;
};

struct sync_context {
	const char		*efivars_dir;
	struct keyset		*firmware_keys;
	struct list_head		new_keys;
};


#define GUID_STRLEN (8 + 1 + 4 + 1 + 4 + 1 + 4 + 1 + 12 + 1)
static void guid_to_str(const EFI_GUID *guid, char *str)
{
	snprintf(str, GUID_STRLEN,
		"%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x",
			guid->Data1, guid->Data2, guid->Data3,
			guid->Data4[0], guid->Data4[1],
			guid->Data4[2], guid->Data4[3],
			guid->Data4[4], guid->Data4[5],
			guid->Data4[6], guid->Data4[7]);
}

static int sha256_key_parse(struct key *key, uint8_t *data, size_t len)
{
	const unsigned int sha256_id_size = 256 / 8;
	unsigned int i;

	if (len != sha256_id_size)
		return -1;

	key->id = talloc_memdup(key, data, sha256_id_size);
	key->id_len = sha256_id_size;

	key->description = talloc_array(key, char, len * 2 + 1);
	for (i = 0; i < len; i++)
		snprintf(&key->description[i*2], 3, "%02x", data[i]);
	key->description[len*2] = '\0';

	return 0;
}

static int x509_key_parse(struct key *key, uint8_t *data, size_t len)
{
	const int description_len = 160;
	ASN1_INTEGER *serial;
	const uint8_t *tmp;
	X509 *x509;
	int rc;

	rc = -1;

	tmp = data;

	x509 = d2i_X509(NULL, &tmp, len);
	if (!x509)
		return -1;

	/* we use the X509 serial number as the key ID */
	if (!x509->cert_info || !x509->cert_info->serialNumber)
		goto out;

	serial = x509->cert_info->serialNumber;

	key->id_len = ASN1_STRING_length(serial);
	key->id = talloc_memdup(key, ASN1_STRING_data(serial), key->id_len);

	key->description = talloc_array(key, char, description_len);
	X509_NAME_oneline(x509->cert_info->subject,
			key->description, description_len);

	rc = 0;

out:
	X509_free(x509);
	return rc;
}

struct cert_type cert_types[] = {
	{ EFI_CERT_SHA256_GUID, sha256_key_parse },
	{ EFI_CERT_X509_GUID, x509_key_parse },
};

static int guidcmp(const EFI_GUID *a, const EFI_GUID *b)
{
	return memcmp(a, b, sizeof(EFI_GUID));
}

static int key_parse(struct key *key, const EFI_GUID *type,
		uint8_t *data, size_t len)
{
	char guid_str[GUID_STRLEN];
	unsigned int i;

	for (i = 0; i < ARRAY_SIZE(cert_types); i++) {
		if (guidcmp(&cert_types[i].guid, type))
			continue;

		key->data = talloc_memdup(key, data, len);
		key->data_len = len;

		return cert_types[i].parse(key, data, len);
	}

	guid_to_str(type, guid_str);
	printf("warning: unknown signature type found:\n  %s\n",
			guid_str);
	return -1;

}

typedef int (*sigdata_fn)(EFI_SIGNATURE_DATA *, int, const EFI_GUID *, void *);

/**
 * Iterates an buffer of EFI_SIGNATURE_LISTs (at db_data, of length len),
 * and calls fn on each EFI_SIGNATURE_DATA item found.
 *
 * fn is passed the EFI_SIGNATURE_DATA pointer, and the length of the
 * signature data (including GUID header), the type of the signature list,
 * and a context pointer.
 */
static int sigdb_iterate(void *db_data, size_t len,
		sigdata_fn fn, void *arg)
{
	EFI_SIGNATURE_LIST *siglist;
	EFI_SIGNATURE_DATA *sigdata;
	unsigned int i, j;
	int rc = 0;

	if (len == 0)
		return 0;

	if (len < sizeof(*siglist))
		return -1;

	for (i = 0, siglist = db_data + i;
			i + sizeof(*siglist) <= len &&
			i + siglist->SignatureListSize > i &&
			i + siglist->SignatureListSize <= len && !rc;
			i += siglist->SignatureListSize,
			siglist = db_data + i) {

		/* ensure that the header & sig sizes are sensible */
		if (siglist->SignatureHeaderSize > siglist->SignatureListSize)
			continue;

		if (siglist->SignatureSize > siglist->SignatureListSize)
			continue;

		if (siglist->SignatureSize < sizeof(*sigdata))
			continue;

		/* iterate through the (constant-sized) signature data blocks */
		for (j = sizeof(*siglist) + siglist->SignatureHeaderSize;
				j < siglist->SignatureListSize && !rc;
				j += siglist->SignatureSize)
		{
			sigdata = (void *)(siglist) + j;

			rc = fn(sigdata, siglist->SignatureSize,
					&siglist->SignatureType, arg);

		}

	}

	return rc;
}

struct keydb_add_ctx {
	struct fs_keystore_entry *ke;
	struct key_database *kdb;
	struct keyset *keyset;
};

static int keydb_add_key(EFI_SIGNATURE_DATA *sigdata, int len,
		const EFI_GUID *type, void *arg)
{
	struct keydb_add_ctx *add_ctx = arg;
	struct key *key;
	int rc;

	key = talloc(add_ctx->keyset, struct key);

	rc = key_parse(key, type, sigdata->SignatureData,
			len - sizeof(*sigdata));

	if (rc) {
		talloc_free(key);
		return 0;
	}
	key->keystore_entry = add_ctx->ke;
	key->type = *type;

	/* add a reference to the keystore entry: we don't want it to be
	 * deallocated if the keystore is deallocated before the
	 * struct key. */
	if (key->keystore_entry)
		talloc_reference(key, key->keystore_entry);

	list_add(&add_ctx->kdb->keys, &key->list);

	return 0;
}

static int read_firmware_keydb(struct sync_context *ctx,
		struct key_database *kdb)
{
	struct keydb_add_ctx add_ctx;
	char guid_str[GUID_STRLEN];
	char *filename;
	uint8_t *buf;
	int rc = -1;
	size_t len;

	add_ctx.keyset = ctx->firmware_keys;
	add_ctx.kdb = kdb;
	add_ctx.ke = NULL;

	guid_to_str(&kdb->type->guid, guid_str);

	filename = talloc_asprintf(ctx->firmware_keys, "%s/%s-%s",
			ctx->efivars_dir, kdb->type->name, guid_str);

	buf = NULL;
	rc = fileio_read_file_noerror(ctx->firmware_keys, filename, &buf, &len);
	if (rc)
		goto out;

	/* efivars files start with a 32-bit attribute block */
	if (len < sizeof(uint32_t))
		goto out;

	buf += sizeof(uint32_t);
	len -= sizeof(uint32_t);

	rc = 0;
	sigdb_iterate(buf, len, keydb_add_key, &add_ctx);

out:
	if (rc)
		talloc_free(buf);
	talloc_free(filename);

	return rc;
}

static int read_keysets(struct sync_context *ctx)
{
	read_firmware_keydb(ctx, &ctx->firmware_keys->pk);
	read_firmware_keydb(ctx, &ctx->firmware_keys->kek);
	read_firmware_keydb(ctx, &ctx->firmware_keys->db);
	read_firmware_keydb(ctx, &ctx->firmware_keys->dbx);

	return 0;
}

static void print_keyset(struct keyset *keyset,
						 const char *name,
						 const char *type,
						 const char *cn)
{
	struct key_database *kdbs[] =
		{ &keyset->pk, &keyset->kek, &keyset->db, &keyset->dbx };
	struct key *key;
	unsigned int i;

	fprintf(stderr, "%s keys:\n", name);

	for (i = 0; i < ARRAY_SIZE(kdbs); i++) {
		if (type && strcmp(type, kdbs[i]->type->name))
			continue;
		fprintf(stderr, "  %s:\n", kdbs[i]->type->name);

		list_for_each(&kdbs[i]->keys, key, list) {
			if (cn && strcmp(cn, key->description))
				continue;
			fprintf(stderr, "    '%s'\n", key->description);
			fwrite(key->data, key->data_len, 1, stdout);
		}
	}
}

static int check_efivars_mount(const char *mountpoint)
{
	struct statfs statbuf;
	int rc;

	rc = statfs(mountpoint, &statbuf);
	if (rc)
		return -1;

	if (statbuf.f_type != EFIVARS_FSTYPE && statbuf.f_type != PSTORE_FSTYPE)
		return -1;

	return 0;
}

static struct keyset *init_keyset(struct sync_context *ctx)
{
	struct keyset *keyset;

	keyset = talloc(ctx, struct keyset);

	list_head_init(&keyset->pk.keys);
	keyset->pk.type = &keydb_types[KEYDB_PK];

	list_head_init(&keyset->kek.keys);
	keyset->kek.type = &keydb_types[KEYDB_KEK];

	list_head_init(&keyset->db.keys);
	keyset->db.type = &keydb_types[KEYDB_DB];

	list_head_init(&keyset->dbx.keys);
	keyset->dbx.type = &keydb_types[KEYDB_DBX];

	return keyset;
}

static struct option options[] = {
	{ "help", no_argument, NULL, 'h' },
	{ "version", no_argument, NULL, 'V' },
	{ "efivars-path", required_argument, NULL, 'e' },
	{ "type", required_argument, NULL, 't' },
	{ "cn", required_argument, NULL, 'c' },
	{ NULL, 0, NULL, 0 },
};

static void usage(void)
{
	printf("Usage: %s [options]\n"
		"Update EFI key databases from the filesystem\n"
		"\n"
		"Options:\n"
		"\t--efivars-path <dir>  Path to efivars mountpoint\n"
		"\t                       (or regular directory for testing)\n",
		toolname);
}

static void version(void)
{
	printf("%s %s\n", toolname, VERSION);
}

int main(int argc, char **argv)
{
	struct sync_context *ctx;
	const char *cn = NULL;
	const char *type = NULL;

	ctx = talloc_zero(NULL, struct sync_context);
	list_head_init(&ctx->new_keys);

	for (;;) {
		int idx, c;
		c = getopt_long(argc, argv, "hVe:t:c:", options, &idx);
		if (c == -1)
			break;

		switch (c) {
		case 'e':
			ctx->efivars_dir = optarg;
			break;
		case 'V':
			version();
			return EXIT_SUCCESS;
		case 'h':
			usage();
			return EXIT_SUCCESS;
		case 'c':
			cn = optarg;
			break;
		case 't':
			type = optarg;
			break;
		}
	}

	if (argc != optind) {
		usage();
		return EXIT_FAILURE;
	}

	ERR_load_crypto_strings();
	OpenSSL_add_all_digests();
	OpenSSL_add_all_ciphers();

	ctx->firmware_keys = init_keyset(ctx);

	if (!ctx->efivars_dir) {
		ctx->efivars_dir = EFIVARS_MOUNTPOINT;
		if (check_efivars_mount(ctx->efivars_dir)) {
			fprintf(stderr, "Can't access efivars filesystem "
					"at %s, aborting\n", ctx->efivars_dir);
			return EXIT_FAILURE;
		}
	}

	read_keysets(ctx);
	print_keyset(ctx->firmware_keys, "firmware", type, cn);

	talloc_free(ctx);

	return EXIT_SUCCESS;
}
